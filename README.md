
# Micro web frameworks   
Este repo contiene un servidor basico implementado en python con el framework Tornado 🌪.



## Authors

- [@martinloera](https://github.com/MartinLoera)

  
## Documentation

[Tornado](https://www.tornadoweb.org/en/stable/index.html)


  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/a319094/tornado-microframework/-/tree/main
```

Go to the project directory

```bash
  cd tornado-microframework
```

Install dependencies

```bash
  pip install tornado
```
Run Tornado server

```bash
  python .\my_server.py
```

In the Web Browser

```bash
http://localhost:8080/helloworld
```

## Tech Stack

**Server:** Tornado, Python

  
## License

[MIT](https://choosealicense.com/licenses/mit/)

  
