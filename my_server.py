import tornado.ioloop
import tornado.web

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Esta es la ruta indice")

class HelloWorld(tornado.web.RequestHandler):
    # def get(self):
    #     self.write("Hello World")
    def get(self):
        self.write('<html><body><form action="/helloworld" method="POST">'
                   '<input type="text" name="message">'
                   '<input type="submit" value="Submit">'
                   '</form></body></html>')

    def post(self):
        self.set_header("Content-Type", "text/plain")
        self.write("Escribiste " + self.get_body_argument("message"))
        
    
app = tornado.web.Application([
    (r'/', IndexHandler),
    (r'/helloworld', HelloWorld)
])

app.listen(8080)
tornado.ioloop.IOLoop.current().start()